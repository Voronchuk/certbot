#/bin/bash -e

DOMAINS="${1:-demo.greet.me matrix.demo.greet.me}"
EMAIL="${EMAIL:-info@strlt.com}"
CERT_NAME="${CERT_NAME:-greet.me}"
SECRET_NAMESPACE="${KUBE_NAMESPACE:-default}"
SECRET_NAME_PREFIX="${SECRET_NAME_PREFIX:-tls-base64}"
SECRET_NAME="${SECRET_NAME_PREFIX}-${CERT_NAME}"
CERT_GEN_ARGS="${CERT_GEN_ARGS:-}"

IFS=" " read -ra domains_arr <<< ${DOMAINS}
DOMAIN_CMD=$(printf " -d %s" "${domains_arr[@]}")

# Replace with --standalone ${DOMAIN_CMD} for live servers --webroot -w "/var/app" ${DOMAIN_CMD} \
echo "Generating certificate ${CERT_NAME} for ${DOMAIN_CMD}"
certbot certonly \
  --email "${EMAIL}" \
  --cert-name "${CERT_NAME}" \
  --standalone ${DOMAIN_CMD} \
  --non-interactive \
  --agree-tos ${CERT_GEN_ARGS} \
  --preferred-challenges http

echo "Generating kubernetes secret ${SECRET_NAME} (namespace ${SECRET_NAMESPACE})"
kubectl create secret generic ${SECRET_NAME} --namespace ${SECRET_NAMESPACE} \
  --from-literal=tls.crt="$(cat /etc/letsencrypt/live/${CERT_NAME}/fullchain.pem | base64)" \
  --from-literal=tls.key="$(cat /etc/letsencrypt/live/${CERT_NAME}/privkey.pem | base64)" \
  --dry-run -o yaml | kubectl apply -f -
  